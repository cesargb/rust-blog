use base::{errors::*,db::DieselErrorExt,state::State};
use actix_web::{ HttpRequest, Responder,HttpResponse, http::header,Path,Json};
use jwt::{encode, decode, Header, Algorithm, Validation};
use user::user_ctrl::{UserCtrl};
use user::user::User;
use base::rest_ctrl::RestCtrl;
use base::app_state::AppState;
use base::app_state::DefaultAppState;

#[derive(Debug, Serialize, Deserialize)]
pub struct LoggedUser {
    pub user_id: i32,
    pub user_type: String
}
impl LoggedUser{
    pub fn user_id(&self) -> i32 {self.user_id}
    pub fn user_type(&self) -> &String {&self.user_type}
}

pub fn create_token(user:&User) -> Result<Token>{
    let c = LoggedUser{user_id:user.id ,user_type:user.user_type.clone()};
    let s = encode(&Header::default(), &c, "secret".as_ref())?;
    println!("{}",s);
    Ok(Token(s))
}
#[derive(Debug, Serialize) ]
pub struct Token(String);
impl Token{
    fn token(&self) -> &str {&self.0}
}
#[derive(Debug, Deserialize)]
pub struct UserLogin{
    email:String,
    pass:String
}


pub struct LoginCtrl;
impl LoginCtrl{
    pub fn login(state:&State,email:&str,pass:&String) -> Result<Token>{
        let user = UserCtrl::get_by_email(&state,&email)
            .map_err(|e| match e.kind() {
                &ErrorKind::NotFound => UserError::Unauthorized.into(),
                _=> e
            })?;
        if user.pass == *pass {
            Ok(create_token(&user)?)
        }else {
            Err(UserError::Unauthorized.into())
        }
    }
    pub fn login_handler( (req, data) : (HttpRequest<DefaultAppState>, Json<UserLogin>)) -> Result<HttpResponse> {

        Ok(HttpResponse::Ok().json(LoginCtrl::login(&State::from_req(&req), &data.email, &data.pass)? ))
    }
    pub fn login_check_handler(req: HttpRequest<DefaultAppState>) -> Result<HttpResponse> {
        let state = State::from_req(&req);
        match state.user {
            Some(_) => Ok(HttpResponse::Ok().json(state.user)),
            None => Ok(HttpResponse::Unauthorized().finish()),
        }
    }

    pub fn decode(token_str:&str,secret:&str) -> Result<LoggedUser>{
        Ok(decode::<LoggedUser>(&token_str, secret.as_ref(), &Validation::default())?.claims)

    }
}
