use user::user::{User,RegUser,NewUser};
use diesel::prelude::*;
use user::login::*;
use ::schema::users::{self, table as Users};
use diesel::{self,insert_into,update,delete};
use base::actix_web_ext::RequestAppStateExt;
use base::{
    errors::*,
    rest_ctrl::*,
    state::State,
    locale::Locale,
    db::DieselErrorExt,
    app_state::AppState
};
use actix_web::HttpRequest;
use base::actix_web_ext::RequestExt;
use base::app_state::DefaultAppState;


pub struct UserCtrl;
impl UserCtrl{
    pub fn get_by_email(state:&State,email: &str) -> Result<User> {
        Users
            .filter( users::email.eq(email) )
            .limit(1)
            .first( &state.conn()? )
            .err_into()
    }
}

impl RestCtrl for UserCtrl{
    type Model = User;
    type NewModel = RegUser;
    type AppState = DefaultAppState;
//    fn before_list(state: &HttpRequest<DefaultAppState>) -> Result<()> {
//        validate_user(state.user(),"adm")
//    }
    perm!(list,"adm");
    fn list_action(state:&HttpRequest<DefaultAppState>) -> Result<Vec<User>>{
        Users.load(&state.conn()? ).err_into()
    }
    perm!(get,"adm");
    fn get_action(state:&HttpRequest<DefaultAppState>,id:i32) -> Result<User>{
        Users.find(id).first(&state.conn()?).err_into()
    }
    fn create_action(state:&HttpRequest<DefaultAppState>,data:RegUser) -> Result<()>{
        insert_into(Users)
            .values(&NewUser::from(data))
            .execute(&state.conn()?)
            .ck_constraints(user_contraints,Locale::from_str(state.locale().unwrap_or("")))?;
        Ok(())

    }

    fn edit_action(state:&HttpRequest<DefaultAppState>,id:i32, data:RegUser) -> Result<()>{
        data.validate()?;
        update(Users.find(id))
            .set( &NewUser::from(data))
            .execute(&state.conn()?)?;
        Ok(())

    }
    fn delete_action(state:&HttpRequest<DefaultAppState>,id:i32) -> Result<()>{
        delete(Users.find(id)).execute(&state.conn()?)?;
        Ok(())
    }
}

fn user_contraints(ck :&str,locale:Locale) -> Option<Error>{
    let msg = match ck {
        "users_min_pass_ck" => "La contraseña debe tener al menos 6 digitos",
        "users_email_unique" => "Este correo ya esta siendo utilizado por otro usuario",
        _=> return None
    };
    Some(UserError::ValidationErr(msg.into()).into())
}

//fn validate_user(u:Option<LoggedUser>, utype:&str) -> Result<()> {
//    match u {
//        Some(ref u) if u.user_type == utype => {
//            Ok(())
//        },
//        Some(_) | None => Err(UserError::Forbidden.into())
//    }
//}

