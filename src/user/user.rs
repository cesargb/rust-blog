use ::schema::*;
use base::errors::*;
use chrono::{NaiveDateTime};
use base::model_data::ModelData;

#[derive(Clone,Debug,Serialize,Deserialize,Queryable)]
pub struct User{
    pub id:i32,
    pub name:String,
    pub email:String,
    pub pass:String,
    pub user_type:String,

    pub created_at:NaiveDateTime,
    pub updated_at:NaiveDateTime,
}

#[derive(Clone,Debug,Deserialize)]

pub struct RegUser{
    pub name:String,
    pub email:String,
    pub pass:String,
    pub pass2:String,
    pub user_type:String
}

impl RegUser{
    pub fn validate(&self) -> Result<()>{
        let mut vec:Vec<&'static str> = vec![];
        if self.pass != self.pass2 {vec.push("contrasenas diferentes")}
        if vec.is_empty() {Ok(())} else {Err(UserError::ValidationErr(vec.join(", ")))? }
    }
    pub fn into_new_user(self) -> NewUser{
        self.into()
    }
}

#[derive(Clone,Debug,Insertable,AsChangeset)]
#[table_name="users"]
pub struct NewUser{
    pub name:String,
    pub email:String,
    pub pass:String,
    pub user_type:String,
}

impl From<RegUser> for NewUser{
    fn from(other: RegUser) -> Self {
        NewUser {
            name: other.name,
            email: other.email,
            pass: other.pass,
            user_type: other.user_type
        }
    }
}

impl ModelData for RegUser{}