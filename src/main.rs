extern crate actix_web;
extern crate serde;
extern crate serde_json;
extern crate toml;
extern crate futures;
extern crate dotenv;
extern crate r2d2;
extern crate r2d2_diesel;
extern crate env_logger;
extern crate listenfd;
extern crate bytes;
extern crate bincode;
extern crate openssl;
extern crate jsonwebtoken as jwt;
extern crate chrono;
#[macro_use] extern crate diesel;
#[macro_use] extern crate serde_derive;
#[macro_use] extern crate failure;
#[macro_use] mod base;
#[macro_use] mod user;
#[macro_use] mod post;
use actix_web::{server, App, HttpRequest, Responder,HttpMessage,HttpResponse, http::{header, HttpTryFrom,NormalizePath}};
use user::user::User;
use futures::future::Future;
use futures::future::ok;
use actix_web::AsyncResponder;
use futures::future::result;
use r2d2_diesel::ConnectionManager;
use base::db::*;
use user::user_ctrl::UserCtrl;
use user::login::LoginCtrl;
use user::user::RegUser;
use post::category_ctrl::CategoryCtrl;
use post::category::NewCategory;
use post::post_ctrl::PostCtrl;
use post::post::NewPost;
use actix_web::Path;
use actix_web::Json;
use base::rest_ctrl::RestCtrl;
use actix_web::middleware::Logger;
mod schema;
use listenfd::ListenFd;
use base::app_state::AppState;
use base::actix_web_ext::ActixAppExt;
use serde::Deserialize;
use openssl::ssl::{SslMethod, SslAcceptor, SslFiletype};
use base::global_config::GlobalConfig;
use base::app_state::DefaultAppState;

//fn content_async(req: HttpRequest<AppState>) -> impl Future<Item=impl Responder,Error=actix_web::error::Error>{
//    let h = accept(&req).to_string();
//    req.json().and_then( move |u:User|{
//        ok(match h.as_ref() {
//            "application/json" => serde_json::to_string(&ping(u)).unwrap(),
//            "application/xml" => toml::to_string(&ping(u)).unwrap(),
//            "" => "nada".to_string(),
//            _ => "otro".to_string()
//        })
//    }).from_err()
//}


fn main() {
    let mut builder = SslAcceptor::mozilla_intermediate(SslMethod::tls()).unwrap();
    builder.set_private_key_file("key.pem", SslFiletype::PEM).unwrap();
    builder.set_certificate_chain_file("cert.pem").unwrap();
    let mut listenfd = ListenFd::from_env();
    std::env::set_var("RUST_LOG", "actix_web=debug");
    env_logger::init();
    let manager = pg_connection_man();

    let pool= r2d2::Pool::builder()
        .max_size(15)
        .build(manager)
        .expect("Failed to create pool.");

    let mut server = server::new(move || {

        App::with_state(DefaultAppState{db:pool.clone(),config: GlobalConfig::new("secret".into())})
            //.middleware(Logger::default())

            .prefix("/api/v1")
           // .resource("/", |r| r.f(greet))
            //.resource("/{name}", |r| r.f(greet))
           // .resource("/content/",|r| r.f(content_handler00))
            //.resource("/content2", |r| r.with_async(content_async11))


            .resource("/login",|r| r.post().with(LoginCtrl::login_handler))
            .resource("/login/check",|r| r.with(LoginCtrl::login_check_handler))
            .rest::<UserCtrl>("/users")
            .rest::<CategoryCtrl>("/categories")
            .rest::<PostCtrl>("/posts")
            .resource("/create",|r| r.get().with(
               |req:HttpRequest<DefaultAppState>| {
                   for i in 1..10{

                       UserCtrl::create(&req,RegUser{
                           name: format!("usuario {}",i),
                           email:format!("usuario{}@mail.com",i) ,
                           pass: "12345".to_string(),
                           pass2: "12345".to_string(),
                           user_type: "adm".to_string(),
                       });
                   }
                   CategoryCtrl::create(&req,NewCategory{
                       category:"general".to_string()
                   });
                   CategoryCtrl::create(&req,NewCategory{
                       category:"programación".to_string()
                   });
                   CategoryCtrl::create(&req,NewCategory{
                       category:"imagenes".to_string()
                   });

                   PostCtrl::create(&req,NewPost{
                       title:"primer post".into(),
                       body:"este es el primer post que escribo por aqui".into(),
                       category_id:1,
                       user_id:1
                   });
                   PostCtrl::create(&req,NewPost{
                       title:"segundo post".into(),
                       body:"este es el segundo post que escribo por aqui".into(),
                       category_id:2,
                       user_id:1
                   });
                   PostCtrl::create(&req,NewPost{
                       title:"tercer post".into(),
                       body:"este es el tercer post que escribo por aqui".into(),
                       category_id:1,
                       user_id:1
                   });
                   ""
               }
            ))


    });
    server = if let Some(l) = listenfd.take_tcp_listener(0).unwrap() {
        server.listen(l)
    } else {
        server.bind("0.0.0.0:8080").expect("Can not bind to port 8080")

    };
    println!("Server running!");
    server.run();

}


