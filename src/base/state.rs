use r2d2;
use diesel::{prelude::PgConnection,r2d2::{ConnectionManager,Pool,PooledConnection}};
use actix_web::{HttpRequest,http::header,HttpMessage};
use base::app_state::AppState;
use user::login::LoggedUser;
use base::errors::*;
use ::std::result::Result as StdResult;
use base::locale::Locale;
use user::login::LoginCtrl;
use base::actix_web_ext::RequestExt;
use base::actix_web_ext::RequestAppStateExt;
pub struct State<'a>{
    pub app:&'a AppState,
    pub user:Option<LoggedUser>,
    pub locale:Locale
}
impl<'a> State<'a>{
    pub fn new(app:&'a impl AppState, user:Option<LoggedUser>,locale:Locale) -> State<'a> {
        State{app,user,locale}
    }
    pub fn db(&self) -> &Pool<ConnectionManager<PgConnection>> { &self.app.db()}
    pub fn conn(&self) -> StdResult<PooledConnection<ConnectionManager<PgConnection>>,r2d2::Error > {self.app.db().get()}

}
impl<'a> State<'a>{
    pub fn from_req(s:&HttpRequest<impl AppState>) -> State{
        state(s)
    }
}
fn state(s:&HttpRequest<impl AppState>)  -> State {
    let locale =  Locale::from_str(s.locale().unwrap_or(""));
    let app = s.state();
    let user = logged_user(&s);
    State{app,user,locale }
}

fn logged_user(req: &HttpRequest<impl AppState>) -> Option<LoggedUser>{
    if let Some(h) = req.headers().get(header::AUTHORIZATION){
        if let Ok(accept) = h.to_str() {
            let mut iter = accept.split_whitespace();
            if iter.next().unwrap_or("").to_lowercase() == "bearer"{
                if let  Some(token) = iter.next(){
                    return LoginCtrl::decode(token,&req.config().jwt_pass).ok()
                }
            }
        }
    }
    None
}