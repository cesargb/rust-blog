use user::login::LoggedUser;
use base::errors::*;
pub mod model_data;
pub mod errors;
pub mod db;
pub mod rest_ctrl;
pub mod locale;
pub mod state;
pub mod app_state;
pub mod actix_web_ext;
pub mod body;
pub mod global_config;

#[macro_export]
macro_rules! diesel_rest {
    ($table:ident) => {
            fn list_action(state: &HttpRequest<Self::AppState>) -> Result<Vec<Self::Model>> {
                use ::schema::$table::dsl::*;
                $table.load(&state.conn()?).err_into()
            }

            fn get_action(state:&HttpRequest<Self::AppState>,id:i32) -> Result<Self::Model> {
               use ::schema::$table::dsl::*;
                $table.find(id).first(&state.conn()?).err_into()
            }

            fn create_action(state: &HttpRequest<Self::AppState>, data: Self::NewModel) -> Result<()> {
                use ::schema::$table::dsl::*;
                diesel::insert_into($table)
                    .values(data)
                    .execute(&state.conn()?)?;
                    //.ck_constraints(user_contraints,Locale::from_str(state.locale().unwrap_or("")))?;
                Ok(())
            }

            fn edit_action(state: &HttpRequest<Self::AppState>, id: i32, data: Self::NewModel) -> Result<()> {
                use ::schema::$table::dsl::*;
                diesel::update($table.find(id))
                    .set( data)
                    .execute(&state.conn()?)?;
                Ok(())
            }

            fn delete_action(state: &HttpRequest<Self::AppState>, id: i32) -> Result<()> {
                use ::schema::$table::dsl::*;
                diesel::delete($table.find(id)).execute(&state.conn()?)?;
                Ok(())
            }
    };
}
#[macro_export]
macro_rules! perm {
    ( list,$($e:expr),*) => {
        fn before_list(state: &HttpRequest<DefaultAppState>) -> Result<()> {
            use base::validate_user;
            $(
                validate_user(state.user(),$e)
            )*
        }
    };
    ( get,$($e:expr),*) => {
        fn before_get(state:&HttpRequest<Self::AppState>,id:&i32) -> Result<()> {
            use base::validate_user;
            $(
                validate_user(state.user(),$e)
            )*
        }
    };
    ( create,$($e:expr),*) => {
        fn before_create(state:&HttpRequest<Self::AppState>,data:&Self::NewModel) -> Result<()> {
            use base::validate_user;
            $(
                validate_user(state.user(),$e)
            )*
        }
    };
    ( edit,$($e:expr),*) => {
        fn before_edit(state:&HttpRequest<Self::AppState>,id:&i32, data:&Self::NewModel) -> Result<()> {
            use base::validate_user;
            $(
                validate_user(state.user(),$e)
            )*
        }
    };
    ( delete,$($e:expr),*) => {
        fn before_delete(state:&HttpRequest<Self::AppState>,id:&i32) -> Result<()> {
            use base::validate_user;
            $(
                validate_user(state.user(),$e)
            )*
        }
    };
}

pub fn validate_user(u:Option<LoggedUser>, utype:&str) -> Result<()> {
    match u {
        Some(ref u) if u.user_type == utype => {
            Ok(())
        },
        Some(_) | None => Err(UserError::Forbidden.into())
    }
}



#[cfg(test)]
pub(crate) mod test_data{
    use base::app_state::AppState;
    use r2d2::Pool;
    use diesel::r2d2::ConnectionManager;
    use diesel::prelude::PgConnection;
    use base::global_config::GlobalConfig;
    use base::model_data::ModelData;
    use actix_web::HttpRequest;
    use base::app_state::DefaultAppState;
    use base::rest_ctrl::RestCtrl;
    use base::errors::*;

    pub struct TestAppState;
    impl AppState for TestAppState{
        fn db(&self) -> &Pool<ConnectionManager<PgConnection>> {
            unimplemented!()
        }

        fn config(&self) -> &GlobalConfig {
            unimplemented!()
        }
    }
    #[derive(Debug,Clone,Serialize,Deserialize)]
    pub struct TestModel{
        pub id:i32
    }
    impl ModelData for TestModel{}

    pub struct RestCtrlTest;
    impl RestCtrl for RestCtrlTest{
        type Model = TestModel;
        type NewModel = TestModel;
        type AppState = TestAppState;
        fn list_action(state:&HttpRequest<TestAppState>) -> Result<Vec<TestModel>>{
            Ok(vec![TestModel{id:1},TestModel{id:3}])
        }
        fn get_action(state:&HttpRequest<TestAppState>,id:i32) -> Result<TestModel>{
            if id==1 {Ok(TestModel{id:1})} else {Err(ErrorKind::NotFound.into())}
        }
        fn create_action(state:&HttpRequest<TestAppState>,data:TestModel) -> Result<()>{
            if data.id==1 {Ok(())} else {Err(UserError::ValidationErr("error".into()).into())}

        }

        fn edit_action(state:&HttpRequest<TestAppState>,id:i32, data:TestModel) -> Result<()>{
            if id==1 && data.id==1 {  Ok(())} else {Err(ErrorKind::NotFound.into())}

        }
        fn delete_action(state:&HttpRequest<TestAppState>,id:i32) -> Result<()>{
            if id==1 {  Ok(())} else {Err(ErrorKind::NotFound.into())}
        }
    }

}

