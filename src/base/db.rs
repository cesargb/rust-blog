use diesel::{self,prelude::{Connection,PgConnection},r2d2::ConnectionManager};
use base::{errors::*,locale::Locale};
use dotenv::dotenv;
use std::env;

pub fn pg_connection() -> PgConnection {
    dotenv().ok();

    let database_url = env::var("DATABASE_URL")
    .expect("DATABASE_URL must be set");
    PgConnection::establish(&database_url)
    .expect(&format!("Error connecting to {}", database_url))
}

pub fn pg_connection_man() -> ConnectionManager<PgConnection> {
    dotenv().ok();

    let database_url = env::var("DATABASE_URL")
        .expect("DATABASE_URL must be set");
    ConnectionManager::<PgConnection>::new(database_url)
}


pub fn ck_constraint(e:diesel::result::Error, ck: impl Fn(&str,Locale) -> Option<Error>, locale:Locale) -> Error {
    if let &diesel::result::Error::DatabaseError(ref e_kind, ref e_info) = &e {
        if let Some(constraint) = e_info.constraint_name() {
            if let Some(error) = ck(constraint,locale) {
                return error
            }
        }
    }
    Error::from(e)
}

pub trait DieselErrorExt<T>:Sized{
    fn ck_constraints<F: Fn(&str,Locale)  -> Option<Error>>(self, op: F, locale:Locale) -> Result<T> ;
}


impl<T> DieselErrorExt<T> for ::std::result::Result<T,diesel::result::Error>{
    fn ck_constraints< F: Fn(&str,Locale) -> Option<Error>>(self, op: F, locale:Locale) -> Result<T> {
        self.map_err(|e| ck_constraint(e,op,locale))
    }
}