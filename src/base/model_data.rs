use base::errors::Result;

pub trait ModelData{
    fn validate(&self) -> Result<()>{Ok(())}

}
//pub trait RestCtrl{
//    type Data;
//    fn list() -> Result<Vec<Data>>;
//    fn get(id:usize) -> Result<Data>;
//    fn create<NT>(data:NT) -> Result<()>;
//    fn edit<NT>(id:usize, data:NT) -> Result<()>;
//    fn delete(id:usize) -> Result<()>;
//}