use r2d2;
use diesel::result as diesel;
use actix_web::error as actix;
use jwt::errors as jwt;
use actix_web::{HttpResponse,http};
use failure::{Fail,Context,Backtrace,SyncFailure};
use std::fmt::{self,Display};

pub type Result<T> = ::std::result::Result<T, Error>;
pub type AsyncResult<T> = ::futures::future::FutureResult<T,Error>;
#[derive(Debug)]
pub struct Error {
    inner: Context<ErrorKind>,
}

#[derive( Debug, Fail)]
pub enum ErrorKind {
    #[fail(display = "Error en el servidor")]
    InternalError,
    #[fail(display = "{} ",_0)]
    User(#[cause] UserError),
    #[fail(display = "Recurso no encontrado")]
    NotFound,

    #[fail(display = "Diesel: {}", _0)]
    Db(#[cause] diesel::Error),
    #[fail(display = "R2D2: {}", _0)]
    DbPool(#[cause] r2d2::Error),
    #[fail(display = " JWT: {}", _0)]
    JWT(#[cause] SyncFailure<jwt::Error>),
}
#[derive( Debug, Fail)]
pub enum UserError{
    #[fail(display = "{}",_0)]
    ValidationErr(String),
    #[fail(display = "Usuario o contraseña no validos")]
    Unauthorized,
    #[fail(display = "No cuenta con permiso para realizar esta acción")]
    Forbidden,
    #[fail(display = "{}",_0)]
    BadRequest(String),


}

impl Fail for Error {
    fn cause(&self) -> Option<&Fail> {
        self.inner.cause()
    }

    fn backtrace(&self) -> Option<&Backtrace> {
        self.inner.backtrace()
    }
}

impl Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        Display::fmt(&self.inner, f)
    }
}
//
impl Error {
    pub fn kind(&self) -> &ErrorKind {
        self.inner.get_context()
    }
    pub fn json(&self) -> String{
        format!("{{\"error\":\"{}\"}}",self)
    }
}

impl UserError {
    pub fn json(&self) -> String{
        format!("{{\"error\":\"{}\"}}",self)
    }
}

impl From<ErrorKind> for Error {
    fn from(kind: ErrorKind) -> Error {
        Error { inner: Context::new(kind) }
    }
}

impl From<UserError> for Error {
    fn from(user_error: UserError) -> Error {
        Error { inner: Context::new(ErrorKind::User(user_error)) }
    }
}

impl From<Context<ErrorKind>> for Error {
    fn from(inner: Context<ErrorKind>) -> Error {
        Error { inner }
    }
}

impl actix::ResponseError for Error {
    fn error_response(&self) -> HttpResponse {
        match *self.kind() {
            ErrorKind::User(ref user_err) =>
                user_err.error_response(),
            ErrorKind::NotFound => HttpResponse::NotFound()
                .content_type("application/json; charset=utf-8").body(self.json()),
            _=> HttpResponse::InternalServerError().finish()
        }
    }
}

impl actix::ResponseError for UserError {
    fn error_response(&self) -> HttpResponse {
        match *self {
            UserError::ValidationErr(ref msg) =>  HttpResponse::BadRequest()
                .content_type("application/json; charset=utf-8").body(self.json()),
            UserError::Unauthorized => HttpResponse::Unauthorized()
                .content_type("application/json; charset=utf-8").body(self.json()),
            //_=> HttpResponse::InternalServerError().finish(),
            UserError::Forbidden => HttpResponse::Forbidden()
                .content_type("application/json; charset=utf-8").body(self.json()),
            UserError::BadRequest(ref msg) => HttpResponse::BadRequest()
                .content_type("application/json; charset=utf-8").body(self.json())
        }
    }
}


impl From<diesel::Error> for Error {
    fn from(err: diesel::Error) -> Error {
        match err {
            diesel::Error::NotFound =>
                Error { inner: Context::new(ErrorKind::NotFound) },
            _ => Error { inner: Context::new(ErrorKind::Db(err)) }
        }


    }
}

impl From<r2d2::Error> for Error {
    fn from(err: r2d2::Error) -> Error {
        Error { inner: Context::new(ErrorKind::DbPool(err)) }
    }
}

impl From<jwt::Error> for Error {
    fn from(err: jwt::Error) -> Error {
        Error { inner: Context::new(ErrorKind::JWT(SyncFailure::new(err))) }
    }
}


pub trait ResultExt<T,E,NE>
    where E :Into<NE>{
    fn err_into(self) -> ::std::result::Result<T,NE>;
}

impl<T,E,NE> ResultExt<T,E,NE> for ::std::result::Result<T,E>
    where NE :From<E>{
    fn err_into(self) -> ::std::result::Result<T, NE> {
        self.map_err(NE::from)
    }
}