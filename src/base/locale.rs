#[derive(Clone,Debug,PartialEq)]
pub enum Locale{
    EsMX,
    EnUS,
    Any,
    Other(String),
}

impl Locale{
    pub fn from_str(s:&str) ->Locale{
        match s.to_lowercase().trim().as_ref() {
            "*" | "" => Locale::Any,
            "es" | "es-mx" => Locale::EsMX,
            "en" | "en-us" => Locale::EnUS,
            _=> Locale::Other(s.to_string())
        }
    }
}
#[cfg(test)]
mod test{
    use super::*;
    #[test]
    fn from_str(){
        assert_eq!(Locale::from_str("es"),Locale::EsMX);
        assert_ne!(Locale::from_str("en"),Locale::EsMX);
        assert_eq!(Locale::from_str(""),Locale::Any);
        assert_eq!(Locale::from_str("*"),Locale::Any);
        assert_eq!(Locale::from_str("ess"),Locale::Other("ess".to_string()));

    }
}