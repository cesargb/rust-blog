use base::rest_ctrl::RestCtrl;
use actix_web::{ App};
use serde::Serialize;
use actix_web::dev::HttpResponseBuilder;
use serde_json;
use toml;
use actix_web::HttpMessage;
use actix_web::{HttpRequest,HttpResponse, http::header,Path,Json};
use base::{errors::*, state::{State}, app_state::AppState };
use bincode;
use r2d2;
use user::login::LoggedUser;
use user::login::LoginCtrl;
use diesel::{prelude::PgConnection,r2d2::{ConnectionManager,Pool,PooledConnection}};
use base::global_config::GlobalConfig;
use ::std::result::Result as StdResult;
use actix_web::test::TestServer;
 use actix_web::FromRequest;
pub trait ActixAppExt<AppSt:AppState>{
    fn rest<C:RestCtrl<AppState=AppSt> + 'static>(mut self, path: &str) -> App<AppSt>;
}
impl<AppSt:AppState + 'static> ActixAppExt<AppSt> for App<AppSt> {
    fn rest<C:RestCtrl<AppState=AppSt> + 'static>(mut self, path: &str) -> App<AppSt>{
        self.scope(path,|r|{
            r.resource("", |r| {
                r.get().with(C::list_handler);
                r.post().with(C::create_handler)
            })
                .resource("/{id}", |r| {
                    r.get().with(C::get_handler);
                    r.put().with(C::edit_handler);
                    r.delete().with(C::delete_handler);
                })
        })
    }
}


pub trait ResponseExt{
    fn parse<T:Serialize>(&mut self, accept:&str , f: impl FnOnce() ->  Result<T>) -> Result<HttpResponse>;
}
impl ResponseExt for HttpResponseBuilder{
    fn parse<T: Serialize>(&mut self, accept:&str, f: impl FnOnce() ->  Result<T>) -> Result<HttpResponse> {
        let body = match accept {
            "application/json" => serde_json::to_vec(&f()?).map_err(|_| Error::from(ErrorKind::InternalError))?,
            "application/toml" => toml::to_vec(&f()?).map_err(|_| Error::from(ErrorKind::InternalError))?,
            "application/bincode" => bincode::serialize(&f()?).map_err(|_| Error::from(ErrorKind::InternalError))?,
            _ => {

                let b = serde_json::to_vec(&f()?).map_err(|_| Error::from(ErrorKind::InternalError))?;
                self.header(header::CONTENT_TYPE, "application/json");
                return Ok(self.body(b));
            }
        };
        self.header(header::CONTENT_TYPE, accept);
        Ok(self.body(body))
    }
}

pub trait RequestExt<T>{
    fn handle<O:Serialize>(&self, f: impl FnOnce() ->  Result<O>) -> Result<HttpResponse>;
    fn accept(&self) -> Option<&str>;
    fn locale(&self) -> Option<&str>;

}

impl<T> RequestExt<T> for HttpRequest<T>{
    fn handle<O: Serialize>(&self, f: impl FnOnce() ->  Result<O>) -> Result<HttpResponse> {
        let ac = self.accept();
        match ac {
            Some(ac) => HttpResponse::Ok().parse(&ac,f),
            None => HttpResponse::Ok().parse("application/json",f)
        }
    }
    fn accept(&self) -> Option<&str>{
        if let Some(h) = self.headers().get(header::ACCEPT){
            if let Ok(accept) = h.to_str() {
                if let Some (val) = accept.split(';').next(){
                    let val = val.trim();
                    return  if !val.is_empty() && val != "*/*"  {
                        Some(val)
                    }else { None }

                }
            }
        }
        None
    }
    fn locale(&self) -> Option<&str>{
        if let Some(h) = self.headers().get(header::ACCEPT_LANGUAGE){
            if let Ok(accept) = h.to_str() {
                if let Some (val) = accept.split(';').next(){
                    return  if !val.is_empty() {Some(val.trim())}
                    else { None }
                }
            }
        }
        None
    }



}

pub trait RequestAppStateExt {
    fn conn(&self) -> StdResult<PooledConnection<ConnectionManager<PgConnection>>,r2d2::Error >;
    fn user(&self) -> Option<LoggedUser>;
    fn config(&self) -> &GlobalConfig;
}
impl<AppSt:AppState> RequestAppStateExt for HttpRequest<AppSt> {

    fn conn(&self) -> StdResult<PooledConnection<ConnectionManager<PgConnection>>,r2d2::Error > {self.state().db().get()}

    fn user(&self) ->Option<LoggedUser>{
        if let Some(h) = self.headers().get(header::AUTHORIZATION){
            if let Ok(accept) = h.to_str() {
                let mut iter = accept.split_whitespace();
                if iter.next().unwrap_or("").to_lowercase() == "bearer"{
                    if let  Some(token) = iter.next(){
                        return LoginCtrl::decode(token,&self.config().jwt_pass).ok()
                    }
                }
            }
        }
        None
    }

    fn config(&self) -> &GlobalConfig {
        &self.state().config()
    }
}






#[cfg(test)]
mod test{
    use super::*;
    use actix_web::test::TestRequest;

    #[test]
    fn request_ext_accept(){
        let resp = TestRequest::with_header("accept", "text/plain").finish();
        let ac = resp.accept();
        assert_eq!(ac.unwrap(), "text/plain");

        let resp = TestRequest::with_header("accept", "*/*").finish();
        let ac = resp.accept();
        assert!(ac.is_none());

        let resp = TestRequest::with_header("accept", "").finish();
        let ac = resp.accept();
        assert!(ac.is_none());

        let resp = TestRequest::default().finish();
        let ac = resp.accept();
        assert!(ac.is_none());
    }

    #[test]
    fn request_ext_locale(){
        let resp = TestRequest::with_header("accept-language", "en;asd").finish();
        let ac = resp.locale();
        assert_eq!(ac.unwrap(), "en");

        let resp = TestRequest::with_header("accept-language", "").finish();
        let ac = resp.locale();
        assert!(ac.is_none());

        let resp = TestRequest::default().finish();
        let ac = resp.locale();
        assert!(ac.is_none());
    }



}