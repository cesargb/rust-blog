use actix_web::Json;
use std::ops::Deref;
use std::ops::DerefMut;
use std::fmt;
use actix_web::FromRequest;
use serde::de::DeserializeOwned;
use actix_web::HttpRequest;
use futures::Future;
use actix_web;
use actix_web::HttpMessage;
use actix_web::dev::JsonConfig;
use futures::future::err;
use base::errors::*;
//use base::bincode::{BinCode,BinCodeConfig};
pub struct Body<T>(pub T);

impl<T> Body<T> {
    /// Deconstruct to an inner value
    pub fn into_inner(self) -> T {
        self.0
    }
}

impl<T> Deref for Body<T> {
    type Target = T;

    fn deref(&self) -> &T {
        &self.0
    }
}

impl<T> DerefMut for Body<T> {
    fn deref_mut(&mut self) -> &mut T {
        &mut self.0
    }
}

impl<T> fmt::Debug for Body<T>
    where
        T: fmt::Debug,
{
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Body: {:?}", self.0)
    }
}

impl<T> fmt::Display for Body<T>
    where
        T: fmt::Display,
{
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        fmt::Display::fmt(&self.0, f)
    }
}


impl<T, S> FromRequest<S> for Body<T>
    where
        T: DeserializeOwned + 'static,
        S: 'static{
    type Config = ();
    type Result = Box<Future<Item = Self, Error = actix_web::Error>>;

    fn from_request(req: &HttpRequest<S>, cfg: &Self::Config) -> Self::Result {
        let content = req.content_type();
        let ret:Box<Future<Item = Body<T>, Error = actix_web::Error>> = match content {
           "application/json" => Box::new(Json::<T>::from_request(req,&JsonConfig::default())
               .map(|e| Body(e.into_inner()))),
//            "application/bincode" => Box::new(BinCode::<T>::from_request(req,&BinCodeConfig::default())
//                .map(|e| Body(e.into_inner()))),
            _=> return Box::new(err(UserError::ValidationErr("content type needed".into()).into()))
        };
        ret

    }
}

//