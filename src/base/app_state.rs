use r2d2::Pool;
use diesel::pg::PgConnection;
use diesel::r2d2::ConnectionManager;
use base::global_config::GlobalConfig;

pub trait AppState{
    fn db(&self) -> &Pool<ConnectionManager<PgConnection>>;
    fn config(&self) -> &GlobalConfig;

}

pub struct DefaultAppState{
    pub db:Pool<ConnectionManager<PgConnection>>,
    pub config:GlobalConfig
}
impl AppState for DefaultAppState{
    fn db(&self) -> &Pool<ConnectionManager<PgConnection>> {
        &self.db
    }

    fn config(&self) -> &GlobalConfig {
        &self.config
    }
}