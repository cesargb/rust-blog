use actix_web::{HttpRequest,HttpResponse, http::header,Path,Json};
use serde::Serialize;
use std::fmt::Debug;
use base::{errors::*, state::{State}, app_state::AppState };
use serde::Deserialize;
use actix_web::dev::HttpResponseBuilder;
use serde_json;
use toml;
use actix_web::HttpMessage;
use base::actix_web_ext::{RequestExt};
use base::body::Body;
use futures::{Future,future::result};
use base::model_data::ModelData;



pub trait RestCtrl{
    type Model:Serialize +Debug ;
    type NewModel: for<'de> Deserialize<'de> + ModelData;
    type AppState:AppState;
    fn before_list(state:&HttpRequest<Self::AppState>) -> Result<()>{Ok(())}
    fn before_get(state:&HttpRequest<Self::AppState>,id:&i32) -> Result<()>{Ok(())}
    fn before_create(state:&HttpRequest<Self::AppState>,data:&Self::NewModel) -> Result<()>{Ok(())}
    fn before_edit(state:&HttpRequest<Self::AppState>,id:&i32, data:&Self::NewModel) -> Result<()>{Ok(())}
    fn before_delete(state:&HttpRequest<Self::AppState>,id:&i32) -> Result<()>{Ok(())}

    fn list_action(state:&HttpRequest<Self::AppState>) -> Result<Vec<Self::Model>>;
    fn get_action(state:&HttpRequest<Self::AppState>,id:i32) -> Result<Self::Model>;
    fn create_action(state:&HttpRequest<Self::AppState>,data:Self::NewModel) -> Result<()>;
    fn edit_action(state:&HttpRequest<Self::AppState>,id:i32, data:Self::NewModel) -> Result<()>;
    fn delete_action(state:&HttpRequest<Self::AppState>,id:i32) -> Result<()>;


    fn list(state:&HttpRequest<Self::AppState>) -> Result<Vec<Self::Model>>{
        Self::before_list(state)?;
        Self::list_action(state)
    }
    fn get(state:&HttpRequest<Self::AppState>,id:i32) -> Result<Self::Model>{
        Self::before_get(state,&id)?;
        Self::get_action(state,id)
    }
    fn create(state:&HttpRequest<Self::AppState>,data:Self::NewModel) -> Result<()>{
        Self::before_create(state,&data)?;
        data.validate()?;
        Self::create_action(state,data)
    }
    fn edit(state:&HttpRequest<Self::AppState>,id:i32, data:Self::NewModel) -> Result<()>{
        Self::before_edit(state,&id,&data)?;
        data.validate()?;
        Self::edit_action(state,id,data)
    }
    fn delete(state:&HttpRequest<Self::AppState>,id:i32) -> Result<()>{
        Self::before_delete(state,&id)?;
        Self::delete_action(state,id)
    }


    fn list_handler(req: HttpRequest<Self::AppState>) ->  Result<HttpResponse> {
        req.handle(||Self::list(&req  ))
    }

    fn get_handler(r:(HttpRequest<Self::AppState>,Path<i32>))->Result<HttpResponse> {
        let (req,p) = r;
        req.handle(||Self::get(&req,p.into_inner()))
    }
    fn get_async(r:(HttpRequest<Self::AppState>,Path<i32>)) -> AsyncResult<HttpResponse>{
        let (req,p) = r;
        result(req.handle(||Self::get(&req,p.into_inner())))
    }


    fn create_handler(r:(HttpRequest<Self::AppState>,Body<Self::NewModel>))-> Result<HttpResponse> {
        let (req,data) = r;
        req.handle(||Self::create(&req ,data.0))
    }
    fn edit_handler(r:(HttpRequest<Self::AppState>,Path<i32>,Body<Self::NewModel>))-> Result<HttpResponse> {
        let (req,p,data) = r;
        req.handle(||Self::edit(&req ,p.into_inner(),data.0))
    }
    fn delete_handler(r:(HttpRequest<Self::AppState>,Path<i32>))-> Result<HttpResponse> {
        let (req,p) = r;
        req.handle(||Self::delete(&req  ,p.into_inner()))
        //Ok(HttpResponse::Ok().finish())
    }
}

#[cfg(test)]
mod test {
    use super::*;
    use actix_web::test::TestRequest;
    use base::test_data::*;
    use actix_web::FromRequest;
    use actix_web::dev::Resource;
    use actix_web::dev::ResourceDef;
    use actix_web::dev::Router;
    #[test]
    fn before_default(){
        let m = TestModel{ id: 123 };
        let req = TestRequest::with_state(TestAppState).finish();
        assert!(RestCtrlTest::before_list(&req).is_ok());
        assert!(RestCtrlTest::before_get(&req,&1).is_ok());
        assert!(RestCtrlTest::before_create(&req,&m).is_ok());
        assert!(RestCtrlTest::before_edit(&req,&1,&m).is_ok());
        assert!(RestCtrlTest::before_delete(&req,&1).is_ok());
    }
    #[test]
    fn actions(){
        let model_good = TestModel{ id: 1 };
        let req = TestRequest::with_state(TestAppState).finish();
        assert!(RestCtrlTest::list_action(&req).is_ok());
        assert!(RestCtrlTest::get_action(&req,1).is_ok());
        assert!(RestCtrlTest::create_action(&req,model_good.clone()).is_ok());
        assert!(RestCtrlTest::edit_action(&req,1,model_good.clone()).is_ok());
        assert!(RestCtrlTest::delete_action(&req,1).is_ok());
    }
    #[test]
    fn methods(){
        let model_good = TestModel{ id: 1 };
        let model_bad = TestModel{ id: 123 };
        let req = TestRequest::with_state(TestAppState).finish();
        assert!(RestCtrlTest::list(&req).is_ok());
        assert!(RestCtrlTest::get(&req,1).is_ok());
        assert!(RestCtrlTest::create(&req,model_good.clone()).is_ok());
        assert!(RestCtrlTest::edit(&req,1,model_good.clone()).is_ok());
        assert!(RestCtrlTest::delete(&req,1).is_ok());

        assert!(!RestCtrlTest::get(&req,123).is_ok());
        assert!(!RestCtrlTest::create(&req,model_bad.clone()).is_ok());
        assert!(!RestCtrlTest::edit(&req,123,model_bad.clone()).is_ok());
        assert!(!RestCtrlTest::delete(&req,123).is_ok());
    }
//    #[test]
//    fn handlers(){
//        let model_good = TestModel{ id: 1 };
//        let model_bad = TestModel{ id: 123 };
//
//        let mut router = Router::<()>::default();
//        router.register_resource(Resource::new(ResourceDef::new("/{id}")));
//
//        let req = TestRequest::with_state(TestAppState)
//            .header("accept", "application/json").uri("/1").finish();
//        let info = router.recognize(&req, &(), 0);
//        let req = req.with_route_info(info);
//
//        assert!(RestCtrlTest::list_handler(req.clone()).is_ok());
//        assert!(RestCtrlTest::get_handler((req.clone(),Path::from_request(&req,&()).unwrap())).is_ok());
//        assert!(RestCtrlTest::create_handler((req.clone(),Body( model_good.clone()))).is_ok());
//        assert!(RestCtrlTest::edit_handler((req.clone(),Path::from_request(&req,&()).unwrap(), Body(model_good.clone()))).is_ok());
//        assert!(RestCtrlTest::delete_handler((req.clone(),Path::from_request(&req,&()).unwrap())).is_ok());
//    }
}



