
pub struct GlobalConfig{
    pub jwt_pass:String
}

impl GlobalConfig{
    pub fn new(jwt_pass:String) -> Self{
        GlobalConfig{jwt_pass}
    }
}