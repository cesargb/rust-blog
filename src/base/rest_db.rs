use actix::prelude::*;
use failure::Error;
use r2d2;
use diesel::pg::PgConnection;
use diesel::r2d2::ConnectionManager;
use std::{thread::sleep, time::Duration};

pub type Pool = r2d2::Pool<ConnectionManager<PgConnection>>;
pub type Connection = r2d2::PooledConnection<ConnectionManager<PgConnection>>;

pub struct DbExecutor(pub Pool);
impl Actor for DbExecutor {
    type Context = SyncContext<Self>;
}

pub enum Queries<T> {
    GetAll,
    Get(i32),
    Create(T),
    Edit(i32,T),
    Delete(i32)
}