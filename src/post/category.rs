use ::schema::*;
use base::errors::*;
use chrono::{NaiveDateTime};
use base::model_data::ModelData;

#[derive(Identifiable,Clone,Debug,Serialize,Deserialize,Queryable)]
#[table_name="categories"]
pub struct Category{
    pub id:i32,
    pub category:String,

    pub created_at:NaiveDateTime,
    pub updated_at:NaiveDateTime,
}

#[derive(Clone,Debug,Insertable,AsChangeset,Deserialize)]
#[table_name="categories"]
pub struct NewCategory{
    pub category:String,
}

impl ModelData for NewCategory{}