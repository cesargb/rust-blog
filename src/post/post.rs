use ::schema::*;
use base::errors::*;
use chrono::{NaiveDate, NaiveDateTime};
use ::user::user::User;
use ::post::category::Category;
use base::model_data::ModelData;

#[derive(Identifiable,Clone,Debug,Associations,Serialize,Deserialize,Queryable)]

pub struct Post{
    pub id:i32,
    pub title:String,
    pub body:String,
    pub category_id:i32,
    pub user_id:i32,

    pub created_at:NaiveDateTime,
    pub updated_at:NaiveDateTime,
}

#[derive(Clone,Debug,Insertable,AsChangeset,Deserialize)]
#[table_name="posts"]
pub struct NewPost{
    pub title:String,
    pub body:String,
    pub category_id:i32,
    pub user_id:i32
}

impl ModelData for NewPost{}