use post::category::{Category,NewCategory};
use diesel::prelude::*;
use diesel::{self,insert_into,update,delete};
use base::actix_web_ext::RequestAppStateExt;
use base::{
    errors::*,
    rest_ctrl::RestCtrl,
    state::State,
    locale::Locale,
    db::DieselErrorExt,
    app_state::AppState
};
use actix_web::HttpRequest;
use base::actix_web_ext::RequestExt;
use base::app_state::DefaultAppState;

pub struct CategoryCtrl;



impl RestCtrl for CategoryCtrl{
    type Model = Category;
    type NewModel = NewCategory;
    type AppState = DefaultAppState;
    perm!(get,"adm");
    diesel_rest!(categories);

}


