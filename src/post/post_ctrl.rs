use post::post::{Post,NewPost};
use diesel::{self,prelude::*};
use base::{
    errors::*,
    rest_ctrl::RestCtrl,
    state::State,
    locale::Locale,
    db::DieselErrorExt,
    app_state::AppState,
    actix_web_ext::RequestAppStateExt
};
use actix_web::HttpRequest;
use base::actix_web_ext::RequestExt;
use base::app_state::DefaultAppState;

pub struct PostCtrl;

impl RestCtrl for PostCtrl{
    type Model = Post;
    type NewModel = NewPost;
    type AppState = DefaultAppState;
    diesel_rest!(posts);

}