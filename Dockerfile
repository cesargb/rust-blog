FROM ubuntu

ENV DEBIAN_FRONTEND=noninteractive
RUN apt-get update && apt-get -y install ca-certificates libssl-dev postgresql && rm -rf /var/lib/apt/lists/*

# copy your source tree
COPY ./ ./




CMD ["./blog"]