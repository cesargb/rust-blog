-- Your SQL goes here
create table users (
  id serial  primary key,
  name varchar(100) not null,
  email varchar(50) not null constraint  users_email_key unique,
  pass varchar(50) not null, -- constraint users_min_pass_ck check (char_length(pass) >= 6)
  user_type varchar(20) not null,

  created_at timestamp not null default now(),
  updated_at timestamp  not null default now()

);

create table images(
    id serial primary key,
    url_image text not null,
    user_id int not null,

  created_at timestamp not null default now(),
  updated_at timestamp  not null default now(),

    constraint image_users_fk
        foreign key (user_id)
        references users (id)
);

create table categories(
    id serial primary key,
    category varchar (20) not null constraint  category_key unique,

    created_at timestamp not null default now(),
    updated_at timestamp  not null default now()

);

create table posts(
    id serial primary key,
    title varchar (150) not null,
    body text not null,
    category_id int not null,
    user_id int not null,

    created_at timestamp not null default now(),
    updated_at timestamp  not null default now(),

    constraint users_categories_fk
        foreign key (category_id)
        references categories (id),

    constraint users_posts_fk
        foreign key (user_id)
        references users (id)

);

create table comments(
    id serial primary key,
    body text not null,
    post_id int not null,
    user_id int not null,

    created_at timestamp not null default now(),
    updated_at timestamp  not null default now(),

    constraint comments_posts_fk
        foreign key (post_id)
        references posts (id),

    constraint comments_users_fk
        foreign key (user_id)
        references users (id)
);